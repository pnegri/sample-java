package com.iugu.sample.anaemic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by patricknegri on 03/03/17.
 */
public class AccountTest {

  private static Log log = LogFactory.getLog(AccountTest.class);

  @BeforeClass
  public static void setup() {
    log.info("Before all tests");

    Locale.setDefault(new Locale("en", "US"));
  }

  @Before
  public void before() {
    log.info("Before a test");
  }

  @After
  public void after() {
    log.info("After a test");
  }

  @AfterClass
  public static void tearDown() {
    log.info("After all tests");
  }

  @Test
  public void testTemplate() {
    log.info("testTemplate");
    assertEquals(1,1);
    assertNotNull(1);
    assertNotEquals(1,0);
    assertTrue(true);

		Account act = new Account();
		assertNotEquals(0, act.getId());
  }

}
