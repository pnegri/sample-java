package com.iugu.sample.anaemic;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Account {
  // Production Environment
  // Test Environment

  @Id
  @GeneratedValue
  @Column(name = "id", columnDefinition = "BINARY(16)")
  private UUID id;

  public Account() {
    // Empty Constructor
  }

  @Override
  public String toString() {
    return "Account{" +
        "id=" + id +
        '}';
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

}
