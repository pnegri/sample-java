package com.iugu.sample.persistance;

import com.iugu.sample.anaemic.Account;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {
  private static Log log = LogFactory.getLog(AccountRepositoryTest.class);

  @Configuration
  @ComponentScan("com.iugu.sample")
  @EntityScan("com.iugu.sample")
  @EnableJpaRepositories("com.iugu.sample")
  public static class TestConfiguration {
  }

  @Autowired
  private AccountRepository accounts;

  @Test
  public void testAccounts() {
    log.info("testAccounts");
    Account acct = new Account();
    accounts.save(acct);

    assertTrue(true);
  }


}
