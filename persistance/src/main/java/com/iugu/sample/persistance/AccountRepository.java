package com.iugu.sample.persistance;

import com.iugu.sample.anaemic.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface AccountRepository extends CrudRepository<Account, UUID> {
}
